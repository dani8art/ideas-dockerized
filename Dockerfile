#Como imagen de partida se utiliza java7.
FROM java:7

#create keystore
RUN /usr/lib/jvm/java-7-openjdk-amd64/jre/bin/keytool -genkey -alias tomcat -keyalg RSA -storepass changeit -dname "cn=Mark Jones, ou=JavaSoft, o=Sun, c=US" -keypass changeit -keystore /opt/keystore

#Prepare tools
RUN apt-get update && apt-get -y install net-tools ssh
RUN yes ideas | adduser ideas-user &> /dev/null

#GET XAMPP
RUN cd /opt && wget https://www.apachefriends.org/xampp-files/5.6.12/xampp-linux-x64-5.6.12-0-installer.run

#INSTALL XAMPP
RUN chmod 755 /opt/xampp-linux-x64-5.6.12-0-installer.run
RUN  ./opt/xampp-linux-x64-5.6.12-0-installer.run

#CREATE IDEAS DATABASE
ADD ./configDB.sh /opt/lampp/
ADD ./ideas_db_dump.sql /opt/lampp/

RUN chmod 755 /opt/lampp/configDB.sh
RUN ./opt/lampp/xampp start && sleep 60s && ./opt/lampp/bin/mysql --user=root < /opt/lampp/configDB.sh

#GET tomcat-7
RUN cd /opt && wget http://ftp.cixug.es/apache/tomcat/tomcat-7/v7.0.69/bin/apache-tomcat-7.0.69.tar.gz 
RUN cd /opt && tar zxf /opt/apache-tomcat-7.0.69.tar.gz

ADD ./tomcat-config/tomcat-users.xml /opt/apache-tomcat-7.0.69/conf/tomcat-users.xml
ADD ./tomcat-config/server.xml /opt/apache-tomcat-7.0.69/conf/server.xml

ADD *.war /opt/apache-tomcat-7.0.69/webapps/
RUN mkdir  /var/local/apache-tomcat-7.0.57
RUN mkdir  /var/local/apache-tomcat-7.0.57/ideas-repo

EXPOSE 8080 8181 80 3306 8009

VOLUME /opt/apache-tomcat-7.0.69/webapps/

CMD ./opt/lampp/xampp start && ./opt/apache-tomcat-7.0.69/bin/catalina.sh run && /etc/init.d/ssh start
