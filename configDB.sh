#! /bin/bash

create database ideas;

create user 'ideasAdmin'@'localhost' IDENTIFIED BY '1d3454dm1n';

grant all privileges on * . * to 'ideasAdmin'@'localhost';

use ideas;

source /opt/lampp/ideas_db_dump.sql
