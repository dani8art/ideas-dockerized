
#! /bin/bash

docker build -t ideas-app .

CONTAINER_ID=$RANDOM

docker run --name=ideas-container-$CONTAINER_ID -d -v /opt/apache-tomcat-7.0.64/webapps:/opt/apache-tomcat-7.0.64/webapps -p 8080:8080 -p 8181:8181 -p 8009:8009 -p 3306:3306 ideas-app

echo "##########INSTRUCTIONS##########" > instructions.txt
echo "" >> instructions.txt

docker inspect ideas-container-$CONTAINER_ID | grep '"IPAddress":' | cut -d : -f2 | awk '{ print $1}' | cut -d "," -f1 | awk '{ print "IPAddress: " $1}' >> instructions.txt

echo "" >> instructions.txt
echo "* To deploy .war: add this to /opt/apache-tomcat-7.0.64/webapps" >> instructions.txt
echo "* Execute: 'ssh ideas-user@IPAddress' for ssh connection" >> instructions.txt
echo "* Execute: 'docker exec -ti ideas-container-$CONTAINER_ID /bin/bash' for docker bash connection" >> instructions.txt
echo "* To stop container execute: docker stop ideas-container-$CONTAINER_ID" >> instructions.txt
echo "* To start container execute: docker start ideas-container-$CONTAINER_ID" >> instructions.txt
echo "" >> instructions.txt
echo "################################" >> instructions.txt

echo ""
cat ./instructions.txt
echo ""
echo "+++ See this instructions on ./instructions.txt +++"